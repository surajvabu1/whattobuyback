import whattobuyback.Member
import whattobuyback.Role

class BootStrap {

    def init = { servletContext ->

        Role role2 = new Role()
        role2.name="Customer"
        role2.save(flush: true,failOnError: true)

        Role role = new Role()
        role.name="Admin"
        role.save(flush: true,failOnError: true)

        Member s = new Member()
        s.firstName = "Suraj"
        s.lastName = "Ghimire"
        s.email = "test1"
        s.password ="test1"
        s.confirmPassword ="suraj1@3"
        s.address = "Gaushala, Kathmandu"
        s.phoneNumber="+9779843550173"
        s.registrationDate = new Date()
        s.cardDetails = null
        s.role=role
        s.save(flush: true,failOnError: true)

        Member customer = new Member()
        customer.firstName = "test"
        customer.lastName = "test"
        customer.email = "test"
        customer.password ="test"
        customer.confirmPassword ="test"
        customer.address = "Gaushala, Kathmandu"
        customer.phoneNumber="+3456345345"
        customer.registrationDate = new Date()
        customer.cardDetails = null
        customer.role=role2
        customer.save(flush: true,failOnError: true)
    }
    def destroy = {
    }
}
