package management

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import whattobuyback.Member
import  whattobuyback.Category

class CategoryManagementController {

    def index() {}

    def saveCategory(){
        if(session.getAttribute("user")!=null){
            Member member = (Member)session.getAttribute("user")
            Member admin = member.get(member.id)
            if(admin.role.name.equals("Admin")){
                Category category = new Category()
                JSONObject requestJson = request.JSON
                String categoryPrimary = requestJson.categoryPrimary;
                String categorySecondary = requestJson.categorySecondary;
                String categoryDescription = requestJson.categoryDescription;

                category.categoryPrimary = categoryPrimary;
                category.categorySecondary = categorySecondary;
                category.categoryDescription = categoryDescription;

                category.save(flush: true, failOnError: true)

                JSONObject object = new JSONObject()
                object.put("connection", "success")
                render object
            }else {
                JSONObject object = new JSONObject()
                object.put("connection", "failed")
                render object
            }
        }
    }

    def categoryList() {
        if(session.getAttribute("user")!=null){
            Member member = (Member)session.getAttribute("user")
            Member admin = member.get(member.id)
            if(admin.role.name.equals("Admin")){
                JSONObject object = new JSONObject()
                JSONArray array = new JSONArray()
                def category = Category.findAll()
                for(Category cat:category) {
                    JSONObject inner = new JSONObject()
                    if((category.id)!=null) {
                        inner.put("id", cat.id)
                        inner.put("categoryPrimary", cat.categoryPrimary)
                        inner.put("categorySecondary", cat.categorySecondary)
                        array.add(inner)
                    }
                }
                object.put("data",array)
                render object
            }
        }
    }
}
