package management

import org.apache.commons.codec.binary.StringUtils
import org.apache.tomcat.util.codec.binary.Base64
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.commons.CommonsMultipartFile

import  whattobuyback.Product
import whattobuyback.Member
import whattobuyback.Color
import whattobuyback.Image
import whattobuyback.Category

import javax.xml.bind.DatatypeConverter

class ProductManagementController {

    def index() {}

    def addProduct(){
        if(session.getAttribute("user")!=null){
            Member member = (Member) session.getAttribute("user")
            Member admin = member.get(member.id)
            if(admin.role.name.equals("Admin")){

//                JSONObject requestJson = request.JSON
//
//                String productName = requestJson.productName
//                String productPrice = requestJson.productPrice
//                String productDescription = requestJson.productDescription
//                String productQuantity = requestJson.productQuantity
//                CommonsMultipartFile file = params.productImage
//
//               // println(requestJson.toString())
//
//                Product product = new Product()
//                product.productName = productName
//                product.productPrice = Double.parseDouble(productPrice)
//                product.productDescription = productDescription
//                String productColor = requestJson.productColor
//                for(String colors:productColor.replace("[","").replace("]","").split(",")) {
//                    Color color = new Color()
//                    color.colorName=colors
//                    product.addToColor(color)
//                }
//                product.productQuantity = Integer.parseInt(productQuantity)
//                product.productImage = file.getBytes()
//                product.addedDate = new Date()
//                product.save(flush: true, failOnError: true)
//
//                JSONObject object = new JSONObject()
//                object.put("connection", "success")
//                render object
                String categoryId = params.categoryList
                categoryId = categoryId.split("_")[0]
                Category category = Category.get(categoryId)
                String productName = params.productName
                String productPrice = params.productPrice
                String productDescription = params.productDescription
                String productQuantity = params.productQuantity
                CommonsMultipartFile file = params.productImage
                String base64Value = params.base64Value
                Product product = new Product()
                product.category = category
                product.productName = productName
                product.productPrice = Double.parseDouble(productPrice)
                product.productDescription = productDescription
                String productColor = params.productColor
                for(String colors:productColor.replace("[","").replace("]","").split(",")) {
                    Color color = new Color()
                    color.colorName=colors
                    product.addToColor(color)
                }
                product.productQuantity = Integer.parseInt(productQuantity)
                Image image = new Image()
                image.productImage = file.getBytes()
                image.base64Value = base64Value
                product.addToImage(image)

                product.addedDate = new Date()
                product.save(flush: true, failOnError: true)

                JSONObject object = new JSONObject()
                object.put("connection", "success")
                render object

            }else {
                JSONObject object = new JSONObject()
                object.put("connection", "failed")
                render object
            }
        }
    }

    def productList() {
        if(session.getAttribute("user")!=null){
            Member member = (Member)session.getAttribute("user")
            Member admin = member.get(member.id)
            if(admin.role.name.equals("Admin")){
                JSONObject object = new JSONObject()
                JSONArray array = new JSONArray()
                def product = Product.findAll()
                for(Product pro:product) {
                    JSONObject inner = new JSONObject()
                    if((product.id)!=null) {
                        inner.put("id", pro.id)
                        inner.put("productName", pro.productName)
                        array.add(inner)
                    }
                }
                object.put("data",array)
                render object
            }
        }
    }

    def viewProduct() {
        if(session.getAttribute("user")!=null){
            Member customer = (Member)session.getAttribute("user")
            Member admin = Member.get(customer.id)
            Product product = Product.get(Integer.parseInt(params.productId))
            //CommonsMultipartFile file =  product.image.productImage

            if(admin.role.name.equals("Admin")){
                JSONObject inner = new JSONObject()
                inner.put("categoryBoth", product.category.categoryPrimary+"-"+product.category.categorySecondary)
                inner.put("productName", product.productName)
                inner.put("productPrice", product.productPrice)
                inner.put("productDescription", product.productDescription)
                inner.put("productColor", product.color.colorName)
                inner.put("productQuantity", product.productQuantity)
//                byte[] abc = product.image.productImage.toString().getBytes()
//                inner.put("productImage", DatatypeConverter.printBase64Binary(abc))

//                byte[] abc = product.image.productImage.toString().bytes
//                StringBuilder sb = new StringBuilder();
//                sb.append("data:image/png;base64,");
//                sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(abc, false)));
//                def imgVal = sb.toString();
                inner.put("productImage", product.image.base64Value)

//                String encoded = product.image.productImage.toString().bytes.encodeBase64().toString()
//                inner.put("productImage", encoded)
                render inner
            }
        }
    }

    def productInformation() {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member admin = Member.get(customer.id)
            if (admin.role.name.equals("Admin")) {
                JSONObject object = new JSONObject()
                JSONArray array = new JSONArray()
                def products = Product.findAll()
                for (Product product : products) {
                    JSONObject inner = new JSONObject()
                    JSONArray colorArray = new JSONArray()
                    inner.put("id", product.id)
                    inner.put("categoryId", product.categoryId+"_"+product.category.categoryPrimary+"-"+product.category.categorySecondary)
                    inner.put("productName", product.productName)
                    inner.put("productPrice", product.productPrice)
                    inner.put("productDescription", product.productDescription)
                    for (Color color : product.color) {
                        colorArray.add(color.colorName + "_" + color.id)
                    }
                    inner.put("color", colorArray)
                    inner.put("productQuantity", product.productQuantity)
                    inner.put("productImage", product.image.base64Value)
                    array.add(inner)
                }
                object.put("data", array)
                render object
            }
        }
    }

    def updateProduct() {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member admin = Member.get(customer.id)
            Product product = Product.get(params.id)
            if(admin.role.name.equals("Admin")){
                String categoryId = params.categoryList
                categoryId = categoryId.split("_")[0]
                Category category = Category.get(categoryId)
                String productName = params.productName
                String productPrice = params.productPrice
                String productDescription = params.productDescription
                String productQuantity = params.productQuantity
                CommonsMultipartFile file = params.productImage
                String base64Value = params.base64Value
                product.category = category
                product.productName = productName
                product.productPrice = Double.parseDouble(productPrice)
                product.productDescription = productDescription
                String colorIds = params.productColor
                for(String colorId:colorIds.replace("[","").replace("]","").split(",")) {
                    boolean check =true;
                    for(Color c: product.color) {
                        if(c.colorName.equalsIgnoreCase(colorId)) {
                            check=false
                        }
                    }
                    if(check) {
                        Color color = new Color()
                        color.colorName=colorId
                        product.addToColor(color)
                    }
                }
                product.productQuantity = Integer.parseInt(productQuantity)
                String imgString = product.image.base64Value
                for(Image imageStatus:product.image){
                    boolean check=true
                    for(Image oldImage:product.image) {
                        if (oldImage.base64Value.equalsIgnoreCase(imgString)) {
                            check false;
                        }
                    }
                    if(check) {
                        imageStatus.productImage = file.getBytes()
                        imageStatus.base64Value = base64Value
                    }
                }

                product.addedDate = new Date()
                product.save(flush: true, failOnError: true)

                JSONObject object = new JSONObject()
                object.put("connection", "success")
                render object

            }else {
                JSONObject object = new JSONObject()
                object.put("connection", "failed")
                render object
            }
        }
    }

    def deleteProduct() {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member admin = Member.get(customer.id)
            Product product = Product.get(params.id)
            if (admin.role.name.equals("Admin")) {
                product.delete(flush: true);
                JSONObject object = new JSONObject()
                object.put("connection","success")
                render object
            }
        }
    }
}
