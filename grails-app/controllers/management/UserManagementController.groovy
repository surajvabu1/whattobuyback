package management

import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import whattobuyback.CardDetails
import whattobuyback.Member

class UserManagementController {

    def index() {}

    def viewMemberDetails = {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member member = Member.get(customer.id)
            JSONObject object = new JSONObject()
            object.put("connection", "success")
            object.put("identity", member.id)
            object.put("firstName", member.firstName)
            object.put("lastName", member.lastName)
            object.put("email", member.email)
            object.put("password", member.password)
            object.put("confirmPassword", member.confirmPassword)
            object.put("address", member.address)
            object.put("phoneNumber", member.phoneNumber)
            render object
        }
    }

    def updateCustomer() {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member member = Member.get(customer.id)
            if (member.role.name.equals("Customer")) {
                JSONObject requestJson = request.JSON
                String firstName = requestJson.firstName
                String lastName = requestJson.lastName
                String email = requestJson.email
                String password = requestJson.password
                String confirmPassword = requestJson.confirmPassword
                String address = requestJson.address
                String phoneNumber = requestJson.phoneNumber
                Member m = Member.get(requestJson.id)
                m.firstName = firstName
                m.lastName = lastName
                m.email = email
                m.password = password
                m.confirmPassword = confirmPassword
                m.address = address
                m.phoneNumber = phoneNumber
                m.save(flush: true, failOnError: true)
                JSONObject object = new JSONObject()
                object.put("connection", "success")
                render object
            }
        }
    }

    def viewBankDetails = {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member member = Member.get(customer.id)
            CardDetails card = member.cardDetails;
            JSONObject object = new JSONObject()
            if (card != null) {
                object.put("connection", "success")
                object.put("cardNumber", card.cardNumber)
                object.put("accountName", card.accountName)
                object.put("securityCode", card.securityCode)
                object.put("issueDate", card.issueDate)
                object.put("expiryDate", card.expiryDate)
                object.put("cardType", card.cardType)
            }
            else {
                object.put("connection", "failed")
            }
            render object
        }
    }

    def updateBankDetails() {
        if (session.getAttribute("user") != null) {
            Member customer = (Member) session.getAttribute("user")
            Member member = Member.get(customer.id)
            if (member.role.name.equals("Customer")) {
                JSONObject requestJson = request.JSON
                if(member.cardDetails != null) {
                    String cardNumber = requestJson.cardNumber
                    String accountName = requestJson.accountName
                    String securityCode = requestJson.securityCode
                    String issueDate = requestJson.issueDate
                    String expiryDate = requestJson.expiryDate
                    String cardType = requestJson.cardType
                    CardDetails card = CardDetails.get(member.cardDetails.id)
                    card.cardNumber = Integer.parseInt(cardNumber)
                    card.accountName = accountName
                    card.securityCode = Integer.parseInt(securityCode)
                    card.issueDate = issueDate
                    card.expiryDate = expiryDate
                    card.cardType = cardType
                    card.save(flush: true, failOnError: true)
                    JSONObject object = new JSONObject()
                    object.put("connection", "success")
                    render object
                }

                else
                {
                    Member.withTransaction {
                        CardDetails carddetails = new CardDetails()
                        //println(requestJson.toString())
                        carddetails.cardNumber = Integer.parseInt(requestJson.cardNumber);
                        carddetails.accountName = requestJson.accountName;
                        carddetails.securityCode = Integer.parseInt(requestJson.securityCode);
                        carddetails.issueDate = requestJson.issueDate;
                        carddetails.expiryDate = requestJson.expiryDate;
                        carddetails.cardType = requestJson.cardType;
                        carddetails.save(flush: true, failOnError: true)
                        member.cardDetails = carddetails
                        member.save(flush: true, failOnError: true)
                        JSONObject object = new JSONObject()
                        object.put("connection","success")
                        render object
                    }
               }
                }
            }
        }
    }
