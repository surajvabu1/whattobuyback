package whattobuyback

import org.codehaus.groovy.grails.web.json.JSONObject

import javax.servlet.http.Cookie

class RegistrationController {

    def index() { }

    def register(){
        Member member = new Member()

        JSONObject requestJson = request.JSON

        // println requestJson.toString()
        // for password validation purpose
        String password = requestJson.password;
        String confirmPassword = requestJson.confirmPassword;

        member.firstName = requestJson.firstName;
        member.lastName = requestJson.lastName;
        member.email = requestJson.email;
        member.password = requestJson.password;
        member.confirmPassword = requestJson.confirmPassword;
        member.address = requestJson.address;
        member.phoneNumber = requestJson.phoneNumber;
        member.registrationDate = new Date();
        member.role = Role.get(1)
        if(password.equals(confirmPassword)){
            member.save(flush: true,failOnError: true)
        }

        JSONObject object = new JSONObject()
        object.put("connection","success")
        render object
    }

    def login(){
//          for GET method
//        println(params.email)
//        println(params.password)

        JSONObject requestJson = request.JSON
        String email = requestJson.email;
        String password = requestJson.password;
        Member member = Member.findByEmailAndPassword(email, password)
        // for get method
//        println("this is my address"+member.address)
//       println("i am called")
        if(member!=null)
        {
            session.setAttribute("user",member);
            JSONObject object = new JSONObject()
            object.put("connection","success")
            object.put("name",member.firstName)
            object.put("role",member.role.name)
            render object
        }

        else {
            JSONObject object = new JSONObject()
            object.put("connection","failed")
            render object
        }
    }


    def bankDetails(){
        if(session.getAttribute("user")!=null){
            Member customer = (Member) session.getAttribute("user")
            Member member = Member.get(customer.id)

            // We need to check for Admin or Customer user here later
            Member.withTransaction {
                CardDetails carddetails = new CardDetails()
                JSONObject requestJson = request.JSON
                //println(requestJson.toString())
                carddetails.cardNumber = Integer.parseInt(requestJson.cardNumber);
                carddetails.accountName = requestJson.accountName;
                carddetails.securityCode = Integer.parseInt(requestJson.securityCode);
                carddetails.issueDate = requestJson.issueDate;
                carddetails.expiryDate = requestJson.expiryDate;
                carddetails.cardType = requestJson.cardType;
                carddetails.save(flush: true, failOnError: true)
                member.cardDetails = carddetails
                member.save(flush: true, failOnError: true)
                JSONObject object = new JSONObject()
                object.put("connection","success")
                render object
            }

        }
    }

    def logout(){
        session.setAttribute("user",null)
        JSONObject object = new JSONObject()
        object.put("connection", "success")
        render object
    }
}