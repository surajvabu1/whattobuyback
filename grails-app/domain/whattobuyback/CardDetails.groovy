package whattobuyback

class CardDetails {
    int cardNumber
    String accountName
    int securityCode
    String issueDate
    String expiryDate
    String cardType

    static constraints = {
        cardNumber unique: true, nullable:false, blank:false
        accountName nullable:false, blank:false
        securityCode nullable:false, blank:false
        issueDate nullable:false, blank:false
        expiryDate nullable:false, blank:false
        cardType nullable:false, blank:false
    }
}
