package whattobuyback

class Category {

    String categoryPrimary
    String categorySecondary
    String categoryDescription

//    static hasMany = [product:Product]

    static constraints = {
        categoryPrimary nullable: false, blank: false
        categorySecondary nullable: false, blank: false
        categoryDescription nullable: true, blank: true
    }

}

