package whattobuyback

class Color {
    String colorName
    static belongsTo = [product:Product]

    static constraints = {
        colorName nullable: false, blank: false
    }
}
