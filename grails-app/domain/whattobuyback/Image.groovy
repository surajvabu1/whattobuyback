package whattobuyback

class Image {

    byte[] productImage
    String base64Value

    static belongsTo = [product:Product]

    static constraints = {

        productImage nullable: false, blank: false, maxSize: 262144 // only set to 256Kb coz browsers add 33% extra on this and max supported for encode base64 value is 512 only
        // max of 4GB 1073741824
        base64Value maxSize: 536870912
    }

}
