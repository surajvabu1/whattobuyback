package whattobuyback

class Member {

    String firstName
    String lastName
    String email
    String password
    String confirmPassword
    String address
    String phoneNumber
    Date registrationDate
    CardDetails cardDetails
    Role role

    static constraints = {
        firstName nullable: false
        email nullable: false, unique: true, blank: false
        password nullable: false, blank: false
        confirmPassword nullable: false
        address nullable: true, blank: false
        phoneNumber unique: true, nullable: true, blank: false
        cardDetails nullable: true, blank: false
    }
}
