package whattobuyback

class Product {
    Category category
    String productName
    Double productPrice
    String productDescription
    int productQuantity
    //byte[] productImage
    Date addedDate

    static hasMany = [color:Color, image:Image]
//    static belongsTo = [category:Category]

    static constraints = {
        productName nullable:false, blank:false
        productPrice nullable:false, blank:false
        productDescription nullable: false, blank: false
        //productImage nullable: false, blank: false, maxSize: 1073741824 // max of 4GB
    }

    static mapping = {
        color cascade:"all-delete-orphan", lazy:false
    }

}
