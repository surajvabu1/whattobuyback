package whattobuyback

class Purchase {
    int quantity
    String discountInPercentage
    String deliveryAddress
    Date purchaseDate
    Date deliveryDate
    boolean isDelivered
    Member member
    Product product

    static constraints = {
        discountInPercentage blank: false
        deliveryAddress blank: true, nullable: true
        deliveryDate blank:false, nullable: true
        isDelivered blank: false, nullable: true
        member nullable: false, blank: false
        product nullable: false, blank:false
    }
}
